'use strict'
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const cors = require('cors')
const CONFIG = require('./config.js');

// example mock data
let data = require('./mock_data.json');
// includes search keywords. you can add more and specify existing ones
let categoryHelpers = require('./categoryHelpers.js');
// include this javascript class in your code to get the items' type
let Category = require('./category.js');

app.use(bodyParser.json());
app.use(cors());

// example loop to get the items' type
data.forEach((item) => {
  let category = new Category(item.name, categoryHelpers.searchKeys);
  category.searchWikipedia().then(() => {
    item.type = category.category ? category.category : 'unknown';
  });
});

// example app which returns the mock data with the items' type
app.get('/', function (req, res) {
  res.send(data);
});

app.listen(CONFIG.PORT, function () {
  console.log(`Example app listening on port ${CONFIG.PORT}!`)
});
