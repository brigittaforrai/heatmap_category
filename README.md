## Heatmap types

**Run the example application**

1. Open terminal
3. Go to the project folder
4. *npm install*
5. For live reload run: *nodemon index.js* or use *npm start* (to install nodemon type: *npm install -g nodemon*)
7. In your browser go to *localhost:8080*



##How to use the script in your code:##

**1. Include in your project then import into your application the following scripts:**

1. category.js
2. categoryHelper.js


**2. npm install:**

1. npm install express *(require in your app)*
2. npm install corse *(require in your app)*
3. npm install bodyParser *(require in your app)*
4. npm install axios
5. npm install wikijs


**3. Use the script:**

```let categoryHelpers = require('./categoryHelpers.js');
let Category = require('./category.js');

let item = {
  name: 'javascrip',
  type: ''
};

let getCategory = new Category(item.name, categoryHelpers.searchKeys);
category.searchWikipedia().then(() => {
  item.type = getCategory.category;
});

console.log(item);```




**Brigitta Forrai**
