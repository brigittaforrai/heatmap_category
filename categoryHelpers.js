let categoryHelpers = {
  searchKeys: [
    { key:'language', category: 'Languages and Frameworks' },
    { key:'platform', category: 'Platforms' },
    { key:'operating system', category: 'Platforms' },
    { key:'tool', category: 'Tools' },
    { key:'technique', category: 'Techniques' },
    { key:'framework', category: 'Languages and Frameworks' },
    { key:'computer', category: 'Tools'},
    { key:'hardware', category: 'Tools'},
    { key:'program', category: 'Tools' },
    { key:'library', category: 'Platforms' },
    { key:'database', category: 'Platforms'},
    { key:'server', category: 'Platforms'},
    { key:'software', category: 'Tools'},
    { key:'application', category: 'Tools'},
    { key:'extension', category: 'Languages and Platforms'},
    { key:'api', category: 'Platforms'},
    { key:'feature', category: 'Techniques'},
    { key:'engine', category: 'Tools'}
  ]
};

module.exports = categoryHelpers;
