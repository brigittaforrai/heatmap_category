const axios = require('axios');
const wiki = require('wikijs').default;

class Category {
  constructor(name, searchHelper) {
    this.name = name;
    this.searchHelper = searchHelper;
    this.wikiSearch = () => {return `https://en.wikipedia.org/w/api.php?action=opensearch&search=${this.name}&limit=50&namespace=0&format=json`};
    this.githubSearch = () => {return `https://api.github.com/search/topics?q=${this.name}`};
    this.tags = [];
    this.category = null;
    this.githubData = [];
  }

  // search github api
  searchGithub () {
    axios.get(this.githubSearch(), {
      headers: {Accept: 'application/vnd.github.mercy-preview+json'}
    }).then((resp) => {
      this.githubData = resp.data.items;
      let index = 0;
      // analize api response (search for keywords)
      while (!this.tags.length && index <= this.githubData.length-1) {
        this.analizeGithubData(index);
        index = index + 1;
      }
      if (this.tags.length) {
        // returns the first category tag. needs more calculations to pick the best one
        this.category = this.tags[0].category;
        return this.category;
      } else {
        // if no result simplify item name and search again
        let splitName = this.name.split(' ')[0];
        if (splitName !== this.name) {
          this.name = splitName;
          return this.searchGithub();
        } else {
          this.category = 'unknown';
          return this.category;
        }
      }
    }).catch(() => {
      // if github search api limit is reached, we get an error
      // search wikipedia instead of github
      wiki().page(this.name).then((page) => {
        page.summary().then((sum) => {
          this.searchHelper.forEach((helper) => {
            if (sum.indexOf(helper.key) !== -1) {
              this.tags.push(helper.key);
            }
          });
          this.category = this.tags[0].category;
        });
      });
    });
  }

  // analize/filter github api response
  analizeGithubData (index) {
    let shortDisc = this.githubData[index].short_description;
    let disc = this.githubData[index].description;
    let dataName = this.githubData[index].name;
    this.tags = this.searchHelper.filter((helper) => {
      return (shortDisc.indexOf(helper.key) !== -1) || (disc.indexOf(helper) !== -1);
    });
    return this;
  }

  // search wikipedia
  searchWikipedia () {
    return axios.get(this.wikiSearch()).then((response) => {
      return this.analizeWikiData(response.data, this.name, 0);
    }).catch((error) => {
      return error;
    });
  }

  // analize and filter wikipedia search api results
  // try to find the right article and analize that
  analizeWikiData (data, name, index) {
    let searchList = data[1];
    let contentList = data[2];

    if (searchList[index]) {
      let content = contentList[index];
      this.tags = this.searchHelper.filter((helper) => {
        return content.includes(helper.key);
      });

      if (this.tags.length) {
        this.category = this.tags[0].category;
        return this.category;
      } else {
        index = index + 1;
        name = searchList[index];
        return this.analizeWikiData(data, name, index);
      }
    } else {
      return this.searchGithub();
    }
  }
};

module.exports = Category;
